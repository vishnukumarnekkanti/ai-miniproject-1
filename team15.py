""" defining the board """ 

def getTargetBlocks(oppoMove):
	possibleBlocks=[[oppoMove[0]-3*(oppoMove[0]/3),oppoMove[1]-3*(oppoMove[1]/3)]]
	curr_Block = possibleBlocks[0]
	if(curr_Block[0]%2==0 and curr_Block[1]%2==0): # Diagonal Case;
		if(curr_Block[0]==0):
			possibleBlocks.append([curr_Block[0]+1,curr_Block[1]])
		else:
			possibleBlocks.append([curr_Block[0]-1,curr_Block[1]])
		if(curr_Block[1]==0):
			possibleBlocks.append([curr_Block[0],curr_Block[1]+1])
		else:
			possibleBlocks.append([curr_Block[0],curr_Block[1]-1])
	return possibleBlocks

def getBlock(board, block):
	pos=[['-' for i in xrange(3)] for j in xrange(3)]
	for i in xrange(0,3):  # Initializing cells of that  block to 	seperate varaible
		for j in xrange(0,3):
			pos[i][j]=board[i+3*block[0]][j+3*block[1]]
	return pos

def setHeuristic(board, flag):# setting heuristic based on number of x and o in this block
	mapper = {}
	mapper['x'] = [135, 210, 285]
	mapper['o'] = [135, 201, 267]
	counter = [[20 for i in xrange(3)] for j in xrange(3)]
	for i in xrange(3):
		string_horiz = board[i][0] + board[i][1] + board[i][2]
		string_vert = board[0][i] + board[1][i] + board[2][i]
		value_horiz = sum(ord(ch) for ch in string_horiz)
		value_vert = sum(ord(ch) for ch in string_vert)
		if value_horiz in mapper[flag]:
			tmp = 3 - mapper[flag].index(value_horiz)
			for j in xrange(3):
				if board[i][j] == '-' :
					counter[i][j] = tmp if tmp < counter[i][j] else counter[i][j]
		if value_vert in mapper[flag]:
			tmp = 3 - mapper[flag].index(value_vert)
			for j in xrange(3):
				if board[j][i] == '-' :
					counter[j][i] = tmp if tmp < counter[j][i] else counter[j][i]
	diag1 = board[0][0] + board[1][1] + board[2][2]
	diag2 = board[0][2] + board[1][1] + board[2][0]
	value_diag1 = sum(ord(ch) for ch in diag1)
	value_diag2 = sum(ord(ch) for ch in diag2)
	if value_diag1 in mapper[flag]:
		tmp = 3 - mapper[flag].index(value_diag1)
		for (i, j) in [(0, 0), (1, 1), (2, 2)]:
			if board[i][j] == '-' :
				counter[i][j] = tmp if tmp < counter[i][j] else counter[i][j]
	if value_diag2 in mapper[flag]:
		tmp = 3 - mapper[flag].index(value_diag2)
		for (i, j) in [(0, 2), (1, 1), (2, 0)]:
			if board[i][j] == '-' :
				counter[i][j] = tmp if tmp < counter[i][j] else counter[i][j]
	least = min([min(counter[0]), min(counter[1]), min(counter[2])])
	return (least, counter)

def evaluate(board, status, block, flag):
	corners = [(0, 0), (0, 2), (2, 0), (2, 2)]
	currBoard = getBlock(board, block)
	nflag = 'x' if flag == 'o' else 'o'
	fn = (setHeuristic(currBoard, flag))[1]
	for i in xrange(3):
		for j in xrange(3):
			if currBoard[i][j] == '-' or (i,j) in corners:
				if (i, j) in corners:
					a = []
					possibleBlocks = getTargetBlocks((i,j))
					for (p, q) in possibleBlocks:
						if status[p][q] == '-' :
							a.append(6-(setHeuristic(getBlock(board,(p,q)),nflag))[0] + (setHeuristic(status, nflag)[0]))
							#a.append(6-(setHeuristic(getBlock(board,(p,q)),nflag))[0] - (setHeuristic(status, flag)[0]))
					if not len(a):
						fn[i][j] += 20 #finished
					else:
						fn[i][j] += max(a)
				else:
					fn[i][j] += 6 - (setHeuristic(getBlock(board, (i, j)), nflag))[0] + (setHeuristic(status, nflag)[0])
					#fn[i][j] += 6 - (setHeuristic(getBlock(board, (i, j)), nflag))[0] - (setHeuristic(status, flag)[0])
			else:
				fn[i][j] += 20 #finished blocks
	final = []
	for i in xrange(3):
		for j in xrange(3):
			final.append((fn[i][j], i+3*block[0], 3*block[1]+j))
	return final

class Player15:
	def __init__(self):
		self.Finished=[]

	def winhardcode(self, pos, flag):  # checking all the 8 possible positions for winning
		if(pos[0][0] == pos[0][1] == pos[0][2] == flag):
			return True
		if(pos[1][0] == pos[1][1] == pos[1][2] == flag):
			return True
		if(pos[2][0] == pos[2][1] == pos[2][2] == flag):
			return True
		if(pos[0][0] == pos[1][0] == pos[2][0] == flag):
			return True
		if(pos[0][1] == pos[1][1] == pos[2][1] == flag):
			return True
		if(pos[0][2] == pos[1][2] == pos[2][2] == flag):
			return True
		if(pos[0][0] == pos[1][1] == pos[2][2] == flag):
			return True
		if(pos[2][0] == pos[1][1] == pos[0][2] == flag):
			return True
		return False

	def win(self, block, board, flag):
		pos = getBlock(board, block)
		for i in xrange(0,3): # checking if there is a possible of winning, if wins, returs a tuple => (True, correspondingpos[0], corres[1])
			for j in xrange(0,3):
				if(pos[i][j] == '-'):
					pos[i][j]=flag
					if(self.winhardcode(pos,flag) == True):
						return (True,i+3*block[0], j+3*block[1])
					pos[i][j]='-'
		return (False,-1,-1) # If there is no possibility of winning, then returns s tuple => (False, -1, -1)

	def move(self, board, status, oppoMove, flag):
		status = [status[:3], status[3:6], status[6:9]]
		temp = getTargetBlocks(oppoMove)
		possibleBlocks=[]
		for i in temp:
			if(status[i[0]][i[1]]=='-'):
				possibleBlocks.append(i)
		if(possibleBlocks==[]):
			for i in xrange(3):
				for j in xrange(3):
					if(status[i][j]=='-') :
						possibleBlocks.append((i,j))
		for block in possibleBlocks:  # see if I can Win, then corresponding pos is returned
			temp = self.win(block, board, flag)
			if(status[block[0]][block[1]] == '-' and temp[0] == True):
				return (temp[1], temp[2])
			temp = self.win(block, board, 'x' if flag == 'o' else 'o')
			if(status[block[0]][block[1]] == '-' and temp[0] == True):
				return (temp[1], temp[2])
		Moves = []
		for block in possibleBlocks :  #Calculates the heuristic function and returnes the position based on that.
			Moves = Moves + evaluate(board, status, block, flag)
		ans = sorted(Moves,key=lambda x:x[0]) # need to verify if the block is already won  or full
		for p in reversed(ans) :
			if(board[p[1]][p[2]] != '-'):
				ans.remove(p)
		return (ans[0][1], ans[0][2])
