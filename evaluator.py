import sys

board=[['-' for i in xrange(9)] for j in xrange(9)]
status=[['-' for i in xrange(3)] for j in xrange(3)]
def getBlock(board, block):
	pos=[['-' for i in xrange(3)] for j in xrange(3)]
	for i in xrange(0,3):  # Initializing cells of that  block to   seperate varaible
		for j in xrange(0,3):
			pos[i][j]=board[i+3*block[0]][j+3*block[1]]
	return pos


def winhardcode(pos, flag):  # checking all the 8 possible pos
	if(pos[0][0] == pos[0][1] == pos[0][2] == flag):
		return True
	if(pos[1][0] == pos[1][1] == pos[1][2] == flag):
		return True
	if(pos[2][0] == pos[2][1] == pos[2][2] == flag):
		return True
	if(pos[0][0] == pos[1][0] == pos[2][0] == flag):
		return True
	if(pos[0][1] == pos[1][1] == pos[2][1] == flag):
		return True
	if(pos[0][2] == pos[1][2] == pos[2][2] == flag):
		return True
	if(pos[0][0] == pos[1][1] == pos[2][2] == flag):
		return True
	if(pos[2][0] == pos[1][1] == pos[0][2] == flag):
		return True
	return False

def out(board, status):
	print "################## GAME BOARD ######################"
	for x in xrange(9):
		for y in xrange(9):
			print board[x][y],
			if(y%3==2):
				print ' ',
		print
		if x%3==2:
			print
	print "####################################################"
	print "################## GAME STATUS #####################"
	for i in xrange(3):
		for j in xrange(3):
			print status[i][j],
		print
	print "####################################################"

def UpdateStatus(board, status):
	for i in xrange(3):
		for j in xrange(3):
			if(status[i][j]=='-'):
				pos = getBlock(board, (i, j))
				if(winhardcode(pos, 'o') == True):
					status[i][j]='o'
				else:
					if(winhardcode(pos, 'x') == True):
						status[i][j]='x'
					else:
						if('-' in pos[0] or '-' in pos[1] or '-' in pos[2]):
							pass
						else:
							status[i][j]='d'
	return status

import team15
import simulator1

P2=team15.Player15()
P1=simulator1.Player1()

prev=(-1, -1)
flag='x'

Next={}
Next['o']='x'
Next['x']='o'

def check(status):
	for i in xrange(3):
		for j in xrange(3):
			if(status[i][j] == '-'):
				return "False"
	return True

while True:
        temp=status[0]+status[1]+status[2]
	m=P1.move(board, temp, prev, flag)
	board[m[0]][m[1]]=flag
	status=UpdateStatus(board, status)
        temp=status[0]+status[1]+status[2]
	out(board, status)
	if(check(status) == True):
		break
	prev=m
	flag=Next[flag]
	m=P2.move(board, temp, prev, flag)
	board[m[0]][m[1]]=flag
	status=UpdateStatus(board, status)
        temp=status[0]+status[1]+status[2]
	out(board, status)
	prev=m
	flag=Next[flag]
	if(check(status) == True):
		break
