""" defining the board """ 
blockHeuristic = [[0 for i in xrange(3)] for j in xrange(3)]

def getTargetBlocks(oppoMove):
	possibleBlocks=[[oppoMove[0]-3*(oppoMove[0]/3),oppoMove[1]-3*(oppoMove[1]/3)]]
	curr_Block = possibleBlocks[0]
	if(curr_Block[0]%2==0 and curr_Block[1]%2==0): # Diagonal Case;
		if(curr_Block[0]==0):
			possibleBlocks.append([curr_Block[0]+1,curr_Block[1]])
		else:
			possibleBlocks.append([curr_Block[0]-1,curr_Block[1]])
		if(curr_Block[1]==0):
			possibleBlocks.append([curr_Block[0],curr_Block[1]+1])
		else:
			possibleBlocks.append([curr_Block[0],curr_Block[1]-1])
	return possibleBlocks

def getBlock(board, block):
	pos=[['-' for i in xrange(3)] for j in xrange(3)]
	for i in xrange(0,3):  # Initializing cells of that  block to 	seperate varaible
		for j in xrange(0,3):
			pos[i][j]=board[i+3*block[0]][j+3*block[1]]
	return pos

def setHeuristic(board, flag):# setting heuristic based on number of x and o in this block
	mapper = {}
	mapper['x'] = [135, 210, 285]
	mapper['o'] = [135, 201, 267]
	counter = [[9 for i in xrange(3)] for j in xrange(3)]
	for i in xrange(3):
		string_horiz = board[i][0] + board[i][1] + board[i][2]
		string_vert = board[0][i] + board[1][i] + board[2][i]
		value_horiz = sum(ord(ch) for ch in string_horiz)
		value_vert = sum(ord(ch) for ch in string_vert)
		if value_horiz in mapper[flag]:
			tmp = 3 - mapper[flag].index(value_horiz)
			for j in xrange(3):
				if board[i][j] == '-' :
					counter[i][j] = tmp if tmp < counter[i][j] else counter[i][j]
		if value_vert in mapper[flag]:
			tmp = 3 - mapper[flag].index(value_vert)
			for j in xrange(3):
				if board[j][i] == '-' :
					counter[j][i] = tmp if tmp < counter[j][i] else counter[j][i]
	diag1 = board[0][0] + board[1][1] + board[2][2]
	diag2 = board[0][2] + board[1][1] + board[2][0]
	value_diag1 = sum(ord(ch) for ch in diag1)
	value_diag2 = sum(ord(ch) for ch in diag2)
	if value_diag1 in mapper[flag]:
		tmp = 3 - mapper[flag].index(value_diag1)
		for (i, j) in [(0, 0), (1, 1), (2, 2)]:
			if board[i][j] == '-' :
				counter[i][j] = tmp if tmp < counter[i][j] else counter[i][j]
	if value_diag2 in mapper[flag]:
		tmp = 3 - mapper[flag].index(value_diag2)
		for (i, j) in [(0, 2), (1, 1), (2, 0)]:
			if board[i][j] == '-' :
				counter[i][j] = tmp if tmp < counter[i][j] else counter[i][j]
	least = min([min(counter[0]), min(counter[1]), min(counter[2])])
	return (least, counter)

def evaluate(board, status, block, flag):
	corners = [(0, 0), (0, 2), (2, 0), (2, 2)]
	currBoard = getBlock(board, block)
	#print currBoard
	fn = (setHeuristic(currBoard, flag))[1]
	for i in xrange(3):
		for j in xrange(3):
			if currBoard[i][j] == '-' or (i,j) in corners:
				if (i, j) in corners:
					a = []
					possibleBlocks = getTargetBlocks((i,j))
					for (p, q) in possibleBlocks:
						if status[p][q] == '-' :
							a.append(3 - (setHeuristic(getBlock(board, (p, q)), 'x' if flag == 'o' else 'o'))[0])
					if(len(a)>0):
						fn[i][j] += max(a)
					else:
						fn[i][j] += 6
				else:
					fn[i][j] += 3 - (setHeuristic(getBlock(board, (i, j)), 'x' if flag == 'o' else 'o'))[0]
			else:
				fn[i][j] += 6 #finished blocks
	final = []
	for i in xrange(3):
		for j in xrange(3):
			final.append((fn[i][j], i+3*block[0], 3*block[1]+j))
	return final

class Player15:
	def __init__(self):
		self.Finished=[]

	def winhardcode(self, pos, flag):  # checking all the 8 possible positions for winning, return true if there is a possibility, else returns False
		if(pos[0][0] == pos[0][1] == pos[0][2] == flag):
			return True
		if(pos[1][0] == pos[1][1] == pos[1][2] == flag):
			return True
		if(pos[2][0] == pos[2][1] == pos[2][2] == flag):
			return True
		if(pos[0][0] == pos[1][0] == pos[2][0] == flag):
			return True
		if(pos[0][1] == pos[1][1] == pos[2][1] == flag):
			return True
		if(pos[0][2] == pos[1][2] == pos[2][2] == flag):
			return True
		if(pos[0][0] == pos[1][1] == pos[2][2] == flag):
			return True
		if(pos[2][0] == pos[1][1] == pos[0][2] == flag):
			return True
		return False

	def win(self, block, board, flag):
		pos = getBlock(board, block)
		for i in xrange(0,3): # checking if there is a possible of winning, if wins, returs a tuple => (True, correspondingpos[0], corres[1])
			for j in xrange(0,3):
				if(pos[i][j] == '-'):
					pos[i][j]=flag
					if(self.winhardcode(pos,flag) == True):
						return (True,i+3*block[0], j+3*block[1])
					pos[i][j]='-'
		return (False,-1,-1) # If there is no possibility of winning, then returns s tuple => (False, -1, -1)

	def move(self, board, status, oppoMove, flag):
		temp = getTargetBlocks(oppoMove)
		possibleBlocks=[]
		for i in temp:
			if(status[i[0]][i[1]]=='-'):
				possibleBlocks.append(i)
		if(possibleBlocks==[]):
			for i in xrange(3):
				for j in xrange(3):
					if(status[i][j]=='-') :
						possibleBlocks.append((i,j))
		for block in possibleBlocks:  # see if I can Win, then corresponding pos is returned
			temp = self.win(block, board, flag)
			if(status[block[0]][block[1]] == '-' and temp[0] == True):
				return (temp[1], temp[2])
			temp = self.win(block, board, 'x' if flag == 'o' else 'o')
			if(status[block[0]][block[1]] == '-' and temp[0] == True):
				return (temp[1], temp[2])
		Moves = []
		for block in possibleBlocks :  #Calculates the heuristic function and returnes the position based on that.
			Moves = Moves + evaluate(board, status, block, flag)
		ans = sorted(Moves,key=lambda x:x[0]) # need to verify if the block is already won  or full
		return (ans[0][1], ans[0][2])
	
	def out(self, board, status):
		print "################## GAME BOARD ######################"
		for x in xrange(9):
			for y in xrange(9):
				print board[x][y],
				if(y%3==2):
					print ' ',
			print
			if x%3==2:
				print
		print "####################################################"
		print "################## GAME STATUS #####################"
		for i in xrange(3):
			for j in xrange(3):
				print status[i][j],
			print
		print "####################################################"

	def validateMove(self, prev, pos, board):
		try:
			board[pos[0]][pos[1]]
		except Exception:
			return False
		if board[pos[0]][pos[1]] != '-' or pos[0] < 0 or pos[1] < 0:
			return False
		else:
			possibleBlocks=[[prev[0]-3*(prev[0]/3),prev[1]-3*(prev[1]/3)]]
			curr_Block= possibleBlocks[0]
			if(curr_Block[0]%2==0 and curr_Block[1]%2==0): # Diagonal Case;
				if(curr_Block[0]==0):
					possibleBlocks.append([curr_Block[0]+1,curr_Block[1]])
				else:
					possibleBlocks.append([curr_Block[0]-1,curr_Block[1]])
				if(curr_Block[1]==0):
					possibleBlocks.append([curr_Block[0],curr_Block[1]+1])
				else:
					possibleBlocks.append([curr_Block[0],curr_Block[1]-1])
				if([pos[0]/3,pos[1]/3] in possibleBlocks):
					return True
				return False
			elif [pos[0]/3,pos[1]/3] in possibleBlocks :
				return True
			else:
				return False

	def UpdateStatus(self, board, status):
		for i in xrange(3):
			for j in xrange(3):
				if(status[i][j]=='-'):
					pos = getBlock(board, (i, j))
					if(self.winhardcode(pos, 'o') == True):
						status[i][j]='o'
					else:
					 	if(self.winhardcode(pos, 'x') == True):
							status[i][j]='x'
						else:
						 	if('-' in pos[0] or '-' in pos[1] or '-' in pos[2]):
								pass
							else:
							 	status[i][j]='d'
		return status
if __name__ == '__main__':
	prev = [-1, -1]
	Gamer = Player15()
	board = [['-' for x in range(9)] for x in range(9)]
	status = [['-' for x in range(3)] for x in range(3)]

	flag = 'x'
	Next = {}
	Next['x'] = 'o'
	Next['o'] = 'x'
	while True:
		Gamer.out(board,status)
		pos = [int(x) for x in raw_input('Enter your move: <format:row column>,(You are playing with : '+flag+')\n').split(' ')]
		if not Gamer.validateMove(prev,pos,board): # It takes an extra argument board,( before board argument was not present so corrected).
			if(prev==[-1, -1]):
				board[pos[0]][pos[1]]=flag
				Gamer.UpdateStatus(board, status)
				flag=Next[flag]
				pos=Gamer.move(board, status,(pos[0],pos[1]),flag)
				board[pos[0]][pos[1]]=flag
				Gamer.UpdateStatus(board, status)
				flag=Next[flag]
				prev=pos
			else:
			 	print "Invalid move"
		else:
			board[pos[0]][pos[1]]=flag
			Gamer.UpdateStatus(board, status)
			flag=Next[flag]
			pos=Gamer.move(board, status,(pos[0], pos[1]),flag)
			board[pos[0]][pos[1]]=flag
			Gamer.UpdateStatus(board, status)
			flag=Next[flag]
			prev=pos
